# Что это?

Пайплайн содержащий джобы для проверок python кода, вынущдающий нас писать код по [PEP8](https://www.python.org/dev/peps/pep-0008/), что есть хорошо.

Пайплайн включающий в себя следующие джобы:

- pylint
- flake
- black

## [pylint](https://pylint.pycqa.org/en/latest/)

```bash
$ find . -type f -name '*.py' -not -path './venv/*' | xargs pylint --disable=E0401                     
************* Module core.urls
core/urls.py:18:0: R0402: Use 'from community import views' instead (consider-using-from-import)
...
************* Module community.views
community/views.py:1:0: C0114: Missing module docstring (missing-module-docstring)
community/views.py:5:0: C0116: Missing function or method docstring (missing-function-docstring)
community/views.py:9:0: C0116: Missing function or method docstring (missing-function-docstring)

------------------------------------------------------------------
Your code has been rated at 7.61/10 (previous run: 7.61/10, +0.00)
```

### .pylintrc (конфиг)
Настоятельно рекомендую при добавлении пайплайна, так же определить в корне (под корнем я подразумеваю контекст, в который приведет `PYTHONCODE_DIR`) `.pylintrc`. Это позволит гибко настроить линтер.

Например можно взять вот [этот](/lint/python/.pylintrc).

## [flake8](https://flake8.pycqa.org/en/latest/)

Еще один линтер.

## [black](https://github.com/psf/black)

Блэк занимается формотирование кода, что уменьшает объем дифов.

## [codeclimate](https://github.com/codeclimate/codeclimate)
Тулза обладающая различными способностями coverage, lint-инг и т.д.

В текущем пайплайне используется для того чтобы строить визуализацию изменения качества кода на основе линтеров.

Подробности [тут](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html).

Как в случае и с `pylint` необходим конфиг, на этот раз **обязательно**.

Например можно взять вот [этот](/lint/python/.pylintrc).
