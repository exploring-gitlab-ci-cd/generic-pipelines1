# Что это?

Пайплайн содержащий линтеры для k8s, статья на [хабре](https://habr.com/ru/company/flant/blog/511018/).

**Зачем?**

Чтобы не пушить / или не пытаться применить не валидные ямлы в k8s.

Чтобы манифесты и чарты соотвествовали хорошим практикам.

## [Kubeval](https://github.com/instrumenta/kubeval)
Валидирует конфиг, точно так же как и `--dry-run`

## [kube-score](https://github.com/zegl/kube-score)
Заставляет вас писать манифесты и чарты соотвествующие хорошим практикам, пример в спойлере.

<details>
<summary>Пример вывода kube-score.</summary>

```bash
batch/v1beta1/CronJob create-django-su in dev                                 💥
    [CRITICAL] Container Resources
        · create-django-su -> CPU limit is not set
            Resource limits are recommended to avoid resource DDOS. Set
            resources.limits.cpu
        · create-django-su -> Memory limit is not set
            Resource limits are recommended to avoid resource DDOS. Set
            resources.limits.memory
        · create-django-su -> CPU request is not set
            Resource requests are recommended to make sure that the application
            can start and run without crashing. Set resources.requests.cpu
        · create-django-su -> Memory request is not set
            Resource requests are recommended to make sure that the application
            can start and run without crashing. Set resources.requests.memory
    [CRITICAL] Container Image Tag
        · create-django-su -> Image with latest tag
            Using a fixed tag is recommended to avoid accidental upgrades
    [CRITICAL] Pod NetworkPolicy
        · The pod does not have a matching NetworkPolicy
            Create a NetworkPolicy that targets this pod to control who/what
            can communicate with this pod. Note, this feature needs to be
            supported by the CNI implementation used in the Kubernetes cluster
            to have an effect.
apps/v1/Deployment crud in dev                                                💥
    [CRITICAL] Pod NetworkPolicy
        · The pod does not have a matching NetworkPolicy
            Create a NetworkPolicy that targets this pod to control who/what
            can communicate with this pod. Note, this feature needs to be
            supported by the CNI implementation used in the Kubernetes cluster
            to have an effect.
    [CRITICAL] Pod Probes
        · Container has the same readiness and liveness probe
            Using the same probe for liveness and readiness is very likely
            dangerous. Generally it's better to avoid the livenessProbe than
            re-using the readinessProbe.
            More information: https://github.com/zegl/kube-score/blob/master/README_PROBES.md
    [CRITICAL] Container Security Context User Group ID
        · mybirds -> Container has no configured security context
            Set securityContext to run the container in a more secure context.
    [CRITICAL] Container Security Context ReadOnlyRootFilesystem
        · mybirds -> Container has no configured security context
            Set securityContext to run the container in a more secure context.
    [CRITICAL] Container Image Tag
        · mybirds -> Image with latest tag
            Using a fixed tag is recommended to avoid accidental upgrades
    [CRITICAL] Deployment has PodDisruptionBudget
        · No matching PodDisruptionBudget was found
            It's recommended to define a PodDisruptionBudget to avoid
            unexpected downtime during Kubernetes maintenance operations, such
            as when draining a node.
    [WARNING] Deployment has host PodAntiAffinity
        · Deployment does not have a host podAntiAffinity set
            It's recommended to set a podAntiAffinity that stops multiple pods
            from a deployment from being scheduled on the same node. This
            increases availability in case the node becomes unavailable.
apps/v1/Deployment db in dev                                                  💥
    [CRITICAL] Pod NetworkPolicy
        · The pod does not have a matching NetworkPolicy
            Create a NetworkPolicy that targets this pod to control who/what
            can communicate with this pod. Note, this feature needs to be
            supported by the CNI implementation used in the Kubernetes cluster
            to have an effect.
    [CRITICAL] Pod Probes
        · Container has the same readiness and liveness probe
            Using the same probe for liveness and readiness is very likely
            dangerous. Generally it's better to avoid the livenessProbe than
            re-using the readinessProbe.
            More information: https://github.com/zegl/kube-score/blob/master/README_PROBES.md
    [CRITICAL] Container Security Context User Group ID
        · pg -> Container has no configured security context
            Set securityContext to run the container in a more secure context.
    [CRITICAL] Container Security Context ReadOnlyRootFilesystem
        · pg -> Container has no configured security context
            Set securityContext to run the container in a more secure context.
    [CRITICAL] Container Image Tag
        · pg -> Image with latest tag
            Using a fixed tag is recommended to avoid accidental upgrades
    [CRITICAL] Deployment has PodDisruptionBudget
        · No matching PodDisruptionBudget was found
            It's recommended to define a PodDisruptionBudget to avoid
            unexpected downtime during Kubernetes maintenance operations, such
            as when draining a node.
    [WARNING] Deployment has host PodAntiAffinity
        · Deployment does not have a host podAntiAffinity set
            It's recommended to set a podAntiAffinity that stops multiple pods
            from a deployment from being scheduled on the same node. This
            increases availability in case the node becomes unavailable.
v1/Service crud in dev                                                        ✅
v1/Service db in dev
```

</details>