# Unreleased

- ...

# 2.0.1

Исправление ошибок

# 2.0.0

Джоба переписана на Пайтоне, находится [здесь](https://gitlab.com/exploring-gitlab-ci-cd/apps/gitlab-api-worker).

# 1.1.1

```bash
MR_ID=$(curl --silent --request GET --header \"PRIVATE-TOKEN: ${MR_VOTE_COUNTER}\" ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/merge_requests | jq ".[]")
```
Из данной коллекции иногда возвращается несколько хэшей коммита, попытка починить так:
```bash
... | jq ".[0]"
```

# 1.1.0

Джоба `check-approve-mr` запускается только на CI_EVENT типа MR.

# 1.0.0

Первый релиз.
