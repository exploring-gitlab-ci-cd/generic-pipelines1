# Что это?

Пост https://habr.com/ru/post/488296/

Добавляет возможность контролировать Merge request (MR), используя обязательный code review.

CI мешает мержить MR, если у него общая сумма лайкой и дизлайков не привышает число `N`.
